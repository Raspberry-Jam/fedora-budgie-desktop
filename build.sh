#!/bin/bash

# Check if a package is installed
check_installed() {
	if rpm -qi $1 > /dev/null 2>& 1; then
		return 0 # Return status 0 for true
	else
		return 1 # Return status 1 for false
	fi
}

ask_to_install() {
	if ! check_installed $1; then
		while true; do
			read -p "$1 was not detected! Install now [Y/n]?" yn
			case $yn in
				[Yy]* ) sudo dnf install $1; break;;
				[Nn]* ) return 1;;
			esac
		done
		return 0
	else
		echo "$1 is detected."
		return 0
	fi
}

budgie_screensaver_spec="budgie-screensaver.spec"
budgie_desktop_spec="budgie-desktop.spec"

if [ ! -f $budgie_screensaver_spec ]; then
	echo "$budgie_screensaver_spec is missing from working directory. Cannot build without it."
fi

if [ ! -f $budgie_desktop_spec ]; then
	echo "$budgie_desktop_spec is missing from working directory. Cannot build without it."
	exit
fi

if ! ask_to_install fedora-packager; then
	echo "Cannot proceed without fedora-packager. Aborting..."
	exit
fi

mkdir -p ./build/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
mkdir -p ./rpms # Built packages moved to here
cp $budgie_screensaver_spec ./build/SPECS/$budgie_screensaver_spec
cp $budgie_desktop_spec ./build/SPECS/$budgie_desktop_spec
cd build
budgie_desktop_spec="./SPECS/$budgie_desktop_spec"
budgie_screensaver_spec="./SPECS/$budgie_screensaver_spec"

# Get version number in screensaver spec file
version="$(cat $budgie_screensaver_spec | grep -oP "Version:\s*\K.*")"

# Install build dependencies for package spec
echo "This requires sudo permissions to install the package dependencies."
sudo dnf builddep $budgie_screensaver_spec

# Grab required source files
wget -O ./SOURCES/budgie-screensaver-$version.tar.xz https://github.com/getsolus/budgie-screensaver/releases/download/$version/budgie-screensaver-$version.tar.xz

# Build the RPM files
rpmbuild --define "_topdir $(pwd)" --define "debug_package %{nil}" -v -bb $budgie_screensaver_spec

# Move built RPM file to rpm dir
mv ./RPMS/x86_64/*.rpm ./../rpms

echo "budgie-screensaver is a build requirement of budgie-desktop. Installing now..."

sudo dnf install ./../rpms/budgie-screensaver*.rpm

# Get version number in desktop spec file
version="$(cat $budgie_desktop_spec | grep -oP "Version:\s*\K.*")"

# Install build dependencies for package spec
echo "This requires sudo permissions to install the package dependencies."
sudo dnf builddep $budgie_desktop_spec

# Grab required source files
wget -O ./SOURCES/budgie-desktop-v$version.tar.xz https://github.com/solus-project/budgie-desktop/releases/download/v$version/budgie-desktop-v$version.tar.xz

# Build the RPM files
rpmbuild --define "_topdir $(pwd)" --define "debug_package %{nil}" -v -bb $budgie_desktop_spec

# Move built RPM files to rpm dir
mv ./RPMS/x86_64/*.rpm ./../rpms