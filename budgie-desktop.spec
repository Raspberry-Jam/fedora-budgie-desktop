# Original spec file from Github user alunux, found at https://github.com/alunux/rpm-specfiles
# Updated for newer versions of Budgie Desktop and Fedora by James Chivers.

%global _hardened_build 1
%global _vpath_builddir build

Name:    budgie-desktop
Version: 10.5.3
Release: 1%{?dist}
License: GPLv2 and LGPLv2.1
Summary: An elegant desktop with GNOME integration
URL:     https://github.com/solus-project/budgie-desktop

Source0: https://github.com/solus-project/budgie-desktop/releases/download/v%{version}/budgie-desktop-v%{version}.tar.xz

BuildRequires: pkgconfig(accountsservice) >= 0.6.40
BuildRequires: pkgconfig(gio-2.0) >= 2.62.0
BuildRequires: pkgconfig(gio-unix-2.0) >= 2.62.0
BuildRequires: pkgconfig(gnome-bluetooth-1.0) >= 3.24.0
BuildRequires: pkgconfig(gnome-desktop-3.0) >= 3.24.0
BuildRequires: pkgconfig(gnome-settings-daemon) >= 3.24.0
BuildRequires: pkgconfig(gobject-2.0) >= 2.62.0
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 1.62.0
BuildRequires: pkgconfig(gtk+-3.0) >= 3.24.0
BuildRequires: pkgconfig(ibus-1.0) >= 1.5.10
BuildRequires: pkgconfig(libgnome-menu-3.0) >= 3.10.3
BuildRequires: pkgconfig(libnotify) >= 0.7
BuildRequires: pkgconfig(libpeas-1.0) >= 1.24.0
BuildRequires: pkgconfig(libpeas-gtk-1.0) >= 1.24.0
BuildRequires: pkgconfig(libpulse) >= 2
BuildRequires: pkgconfig(libpulse-mainloop-glib) >= 2
BuildRequires: pkgconfig(libwnck-3.0) >= 3.32.0
%if 0%{?fedora} == 28
BuildRequires: pkgconfig(libmutter-2) >= 3.28.0
%endif
%if 0%{?fedora} == 29
BuildRequires: pkgconfig(libmutter-3) >= 3.30.0
%endif
%if 0%{?fedora} == 30
BuildRequires: pkgconfig(libmutter-4) >= 3.32.0
%endif
%if 0%{?fedora} == 31
BuildRequires: pkgconfig(libmutter-5) >= 3.34.0
%endif
%if 0%{?fedora} == 32
BuildRequires: pkgconfig(libmutter-6) >= 3.36.0
%endif
%if 0%{?fedora} >= 33
BuildRequires: pkgconfig(libmutter-7) >= 3.38.0
%endif
BuildRequires: pkgconfig(polkit-agent-1) >= 0.110
BuildRequires: pkgconfig(polkit-gobject-1) >= 0.110
BuildRequires: pkgconfig(upower-glib) >= 0.99.0
BuildRequires: pkgconfig(uuid)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(gsettings-desktop-schemas)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(xtst)
BuildRequires: pkgconfig(alsa)

BuildRequires: vala >= 0.40.0
BuildRequires: git
BuildRequires: meson
BuildRequires: intltool
BuildRequires: gtk-doc
BuildRequires: sassc
BuildRequires: desktop-file-utils
BuildRequires: mesa-libEGL-devel
BuildRequires: make

Requires: hicolor-icon-theme
Requires: gnome-session
Requires: gnome-settings-daemon
Requires: control-center
Requires: network-manager-applet
Requires: budgie-screensaver
Requires: mutter

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

Requires: %{name}-libs
Requires: %{name}-plugins-core
Requires: %{name}-schemas
Requires: %{name}-rundialog

Recommends: adapta-gtk-theme
Recommends: pop-icon-theme

%description
Budgie is the flagship desktop of the Solus, and is an Solus project.
The Budgie Desktop a modern desktop designed to keep out the way of
the user. It features heavy integration with the GNOME stack in order
for an enhanced experience.


%package        plugins-core
Summary:        Core plugins for the Budgie Desktop
Requires:       gtk3 >= 3.24.0
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Requires:       %{name}-schemas%{?_isa} = %{version}-%{release}

%description    plugins-core
This package contains the core plugins of Budgie Desktop.


%package        rundialog
Summary:        Budgie Run Dialog for the Budgie Desktop
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Requires:       %{name}-schemas%{?_isa} = %{version}-%{release}

%description    rundialog
Budgie Run Dialog for the Budgie Desktop


%package        libs
Summary:        Common libs for the Budgie Desktop
Requires:       gtk3 >= 3.24.0

%description    libs
This package contains the shared library of Budgie Desktop.


%package        schemas
Summary:        GLib schemas for the Budgie Desktop

%description    schemas
GLib schemas for the Budgie Desktop


%package        docs
Summary:        GTK3 Desktop Environment -- Documentation files
Group:          Documentation/HTML

%description    docs
GTK3 Desktop Environment -- Documentation files.
This package provides API Documentation for the Budgie Plugin API, in the
GTK-Doc HTML format.


%package        devel
Summary:        Development files for the Budgie Desktop
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains the files required for developing for Budgie Desktop.


%prep
%setup -q -T -b 0 -n %{name}-%{version}
if [ ! -d .git ]; then
    git clone --bare --depth 1 https://github.com/solus-project/budgie-desktop.git .git
    git config --local --bool core.bare false
    git reset --hard
fi

%build
export LC_ALL=en_US.utf8
%if 0%{?fedora} < 28
%meson
%else
%meson -Dwith-desktop-icons=none
%endif
%meson_build


%install
export LC_ALL=en_US.utf8
%meson_install
find %{buildroot} -name '*.la' -delete

%find_lang %{name}


%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/budgie-*.desktop


%ldconfig_scriptlets libs


%files -f %{name}.lang
%doc README.md
%license LICENSE LICENSE.LGPL2.1
%{_bindir}/budgie-*
%config(noreplace) %{_sysconfdir}/xdg/autostart/budgie-desktop-*.desktop
%{_datadir}/applications/budgie-*.desktop
%{_datadir}/gnome-session/sessions/budgie-desktop.session
%{_datadir}/icons/hicolor/scalable/apps/budgie-desktop-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/notification-alert-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/pane-hide-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/pane-show-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/system-hibernate-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/system-log-out-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/system-restart-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/system-suspend-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/clock-applet-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/icon-task-list-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/notifications-applet-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/notification-disabled-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/separator-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/spacer-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/system-tray-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/task-list-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/workspace-switcher-symbolic.svg
%{_datadir}/icons/hicolor/scalable/apps/my-caffeine-on-symbolic.svg
%{_datadir}/icons/hicolor/scalable/status/budgie-caffeine-cup-empty.svg
%{_datadir}/icons/hicolor/scalable/status/budgie-caffeine-cup-full.svg
%{_datadir}/icons/hicolor/scalable/status/caps-lock-symbolic.svg
%{_datadir}/icons/hicolor/scalable/status/num-lock-symbolic.svg
%{_datadir}/xsessions/budgie-desktop.desktop
%{_datadir}/backgrounds/budgie/default.jpg

%files plugins-core
%{_libdir}/budgie-desktop/*

%files schemas
%{_datadir}/glib-2.0/schemas/com.solus-project.*.gschema.xml
%if 0%{?fedora} >= 29
%{_datadir}/glib-2.0/schemas/20_solus-project.budgie.wm.gschema.override
%endif

%files libs
%{_libdir}/budgie-desktop/
%{_libdir}/libbudgietheme.so.0
%{_libdir}/libbudgietheme.so.0.0.0
%{_libdir}/libbudgie-plugin.so.0
%{_libdir}/libbudgie-plugin.so.0.0.0
%{_libdir}/libbudgie-private.so.0
%{_libdir}/libbudgie-private.so.0.0.0
%{_libdir}/libraven.so.0
%{_libdir}/libraven.so.0.0.0
%{_libdir}/girepository-1.0/Budgie*.typelib

%files rundialog
%{_bindir}/budgie-run-dialog

%files docs
%{_datadir}/gtk-doc/html/budgie-desktop/

%files devel
%{_includedir}/budgie-desktop/
%{_libdir}/pkgconfig/budgie*.pc
%{_libdir}/libbudgietheme.so
%{_libdir}/libbudgie-plugin.so
%{_libdir}/libbudgie-private.so
%{_libdir}/libraven.so
%{_datadir}/gir-1.0/Budgie-1.0.gir
%{_datadir}/vala/vapi/budgie-1.0.*


%changelog
* Thu Nov 05 2020 James Chivers <raspberryjam.chivers@gmail.com> - 10.5.1-1
- build from release 10.5.1
