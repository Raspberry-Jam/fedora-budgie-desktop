#!/bin/bash

# Check if a package is installed
check_installed() {
	if rpm -qi $1 > /dev/null 2>& 1; then
		return 0 # Return status 0 for true
	else
		return 1 # Return status 1 for false
	fi
}

# Get the package suffix of built rpm file
get_rpm_suffix() {
	if check_installed $1; then
		version="$(rpm -qi $1 | grep -oP "Version\s*:\s\K.*")"
		release="$(rpm -qi $1 | grep -oP "Release\s*:\s\K.*")"
		arch="$(rpm -qi $1 | grep -oP "Architecture\s*:\s\K.*")"
		echo "$version-$release.$arch"
	fi
}

cd ./rpms

# Get version suffix of built budgie-screensaver rpm file
ver="$(get_rpm_suffix $(ls | grep -m 1 "budgie-screensaver"))"

# Install budgie-screensaver for budgie-desktop build dependency
sudo dnf install budgie-screensaver-$ver.rpm

# Remove user installed mark from dependency packages to ensure it is uninstalled with the main package
sudo dnf mark remove budgie-screensaver

# Get version suffix of all built budgie-desktop rpm files
ver="$(get_rpm_suffix $(ls | grep -m 1 "budgie-desktop"))"

# Install packages
sudo dnf install budgie-desktop-libs-$ver.rpm \
				 budgie-desktop-plugins-core-$ver.rpm \
				 budgie-desktop-schemas-$ver.rpm \
				 budgie-desktop-rundialog-$ver.rpm \
				 budgie-desktop-$ver.rpm

# Remove user installed mark from dependency packages to ensure they are uninstalled with the main package
sudo dnf mark remove budgie-desktop-libs \
					 budgie-desktop-plugins-core \
					 budgie-desktop-schemas \
					 budgie-desktop-rundialog
