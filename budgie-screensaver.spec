Name: budgie-screensaver
Version: v4.0
Release: 1%{?dist}
License: GPLv2
Summary: Lock and unlock for the Budgie Desktop environment
URL: https://github.com/solus-project/budgie-screensaver

Source0: https://github.com/getsolus/budgie-screensaver/releases/download/%{version}/budgie-screensaver-%{version}.tar.xz

BuildRequires: intltool
BuildRequires: gcc
BuildRequires: glib2-devel
BuildRequires: dbus-glib-devel
BuildRequires: gnome-desktop3-devel
BuildRequires: pam-devel

Requires: dbus-glib
Requires: gnome-desktop3
Requires: libICE

%description
budgie-screensaver is a fork of gnome-screensaver for the Budgie Desktop environment

%prep
%autosetup

%build
./configure
make

%install
%make_install

%files
%{_prefix}/local/bin/budgie-screensaver
%{_prefix}/local/bin/budgie-screensaver-command
%{_prefix}/local/etc/pam.d/budgie-screensaver
%{_prefix}/local/libexec/budgie-screensaver-dialog
%{_prefix}/local/share/applications/budgie-screensaver.desktop
%{_prefix}/local/share/locale/af/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/an/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ar/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/as/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ast/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/be/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/be@latin/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/bg/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/bn/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/bn_IN/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/br/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/bs/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ca/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ca@valencia/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/crh/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/cs/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/cy/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/da/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/de/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/dz/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/el/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/en_CA/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/en_GB/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/en@shaw/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/eo/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/es/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/et/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/eu/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/fa/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/fi/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/fr/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/fur/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ga/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/gd/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/gl/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/gu/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/he/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/hi/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/hr/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/hu/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/id/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/is/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/it/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ja/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ka/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/km/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/kn/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ko/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ku/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/lt/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/lv/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/mai/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/mg/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/mk/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ml/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/mn/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/mr/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ms/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/my/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/nb/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/nds/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ne/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/nl/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/nn/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/oc/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/or/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/pa/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/pl/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ps/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/pt/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/pt_BR/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ro/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ru/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/si/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sk/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sl/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sq/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sr/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sr@latin/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/sv/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ta/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/te/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/th/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/tr/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/ug/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/uk/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/vi/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/xh/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/zh_CN/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/zh_HK/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/locale/zh_TW/LC_MESSAGES/budgie-screensaver.mo
%{_prefix}/local/share/man/man1/budgie-screensaver.1
%{_prefix}/local/share/man/man1/budgie-screensaver-command.1